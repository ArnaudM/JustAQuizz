package com.orange.quizz.manager;

import android.content.Context;

/**
 * Created by LHMF8286 on 24/11/2017.
 */

public class PreferenceManager {

    public static final String PREFERENCES_FILE = "prefs";

    public static final String HIGH_SCORE_KEY = "highScore";

    public static int getHighScore(Context context) {
        return context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE).getInt(HIGH_SCORE_KEY, 0);
    }

    public static void updateHighScore(Context context, int score) {
        int lastScore = getHighScore(context);

        if (score > lastScore) {
            context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE)
                    .edit()
                    .putInt(HIGH_SCORE_KEY, score)
                    .apply();
        }

    }
}
