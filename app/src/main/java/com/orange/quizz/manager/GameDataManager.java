package com.orange.quizz.manager;

import android.content.Context;
import android.widget.Toast;

import com.orange.quizz.model.Quizz;
import com.orange.quizz.model.QuizzClient;
import com.orange.quizz.util.Constants;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by LHMF8286 on 21/11/2017.
 */

public class GameDataManager {

    private static GameDataManager ourInstance = null;

    private static Quizz intermediateQuestions;
    private static Quizz expertQuestions;
    private static Quizz simpleQuestions;

    public static Quizz getSimpleQuestions() {
        return simpleQuestions;
    }
    public static Quizz getIntermediateQuestions() {
        return intermediateQuestions;
    }
    public static Quizz getExpertQuestions() {
        return expertQuestions;
    }

    private ILoaderListener listener;

    private QuizzClient quizzClient;
    private OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private int nbRequests = 0;

    public void initDatabases(Context context, ILoaderListener listener) {
        appContext = context.getApplicationContext();

        this.listener = listener;
        loadAsyncTask();
    }

    public static synchronized GameDataManager getInstance() {
        if (ourInstance == null) {
            ourInstance = new GameDataManager();
        }
        return ourInstance;
    }

    private GameDataManager() {

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Constants.WS_URL)
                .addConverterFactory(GsonConverterFactory.create());


        Retrofit retrofit = builder
                .client(httpClient.build())
                .build();

        quizzClient = retrofit.create(QuizzClient.class);
    }


    private Context appContext;

    private void loadAsyncTask() {
        quizzClient.listSimpleQuestions().enqueue(new Callback<Quizz>() {
            @Override
            public void onResponse(Call<Quizz> call, Response<Quizz> response) {
                simpleQuestions = response.body();
                checkProgress();
            }

            @Override
            public void onFailure(Call<Quizz> call, Throwable t) {
                Toast.makeText(appContext, "Failed to download SimpleQuestions", Toast.LENGTH_SHORT).show();
            }
        });

        quizzClient.listNormalQuestions().enqueue(new Callback<Quizz>() {
            @Override
            public void onResponse(Call<Quizz> call, Response<Quizz> response) {
                intermediateQuestions = response.body();
                checkProgress();
            }

            @Override
            public void onFailure(Call<Quizz> call, Throwable t) {
                Toast.makeText(appContext, "Failed to download NormalQuestions", Toast.LENGTH_SHORT);
            }
        });

        quizzClient.listExpertQuestions().enqueue(new Callback<Quizz>() {
            @Override
            public void onResponse(Call<Quizz> call, Response<Quizz> response) {
                expertQuestions = response.body();
                checkProgress();
            }

            @Override
            public void onFailure(Call<Quizz> call, Throwable t) {
                Toast.makeText(appContext, "Failed to download ExpertQuestions", Toast.LENGTH_SHORT);
            }
        });
    }

    private void checkProgress(){
        nbRequests ++;

        if(listener != null) {
            listener.onProgressChanged(nbRequests);
        }

        if(nbRequests == 3) {
            listener.onLoaded();
        }
    }

    /*
    public void launchDatabaseRetrieving(final Handler handler, final int key, final Context context) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                initDatabases(context);
                Message progress = handler.obtainMessage(key, " loaded !");
                progress.sendToTarget();
            }
        });
        thread.start();
    }*/

    /*public Quizz loadQuestions(Context context, int resId) {

        Quizz res = null;
        Gson gson = new Gson();

        InputStream is = context.getResources().openRawResource(resId);

        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            res = gson.fromJson(reader, Quizz.class);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return res;
    }*/

    public interface ILoaderListener {
        void onProgressChanged(int nbReq);
        void onLoaded();
    }

}
