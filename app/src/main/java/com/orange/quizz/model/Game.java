package com.orange.quizz.model;

import android.os.CountDownTimer;

import com.orange.quizz.util.Constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;

/**
 * Created by LHMF8286 on 20/11/2017.
 */

public class Game {

    private int score = 0;
    private ArrayList<Question> questions = new ArrayList();
    private Level level = Level.BEGINNER;
    private int index = -1;
    private Long duration = 0L;
    private boolean isRunning = false;
    private Date startQuestionTime;

    private IGameListener listener;

    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
    }
    public int getScore() {
        return score;
    }

    private CountDownTimer timer = new CountDownTimer(Constants.QUESTION_DURATION_MS, 1000) {
        @Override
        public void onTick(long l) {

            if (listener != null) {
                listener.onTick((int)(l/1000));
            }
        }

        @Override
        public void onFinish() {
            onAnswer(null);
        }
    };

    private void initGame() {
        questions.clear();

        for (int i = 0; i < Constants.NB_QUESTION; i++) {
            Question question = new Question();
            question.setStory("Ceci est l'anecdote de la question " + i);
            question.setQuestion("Ceci est la question " + i);
            ArrayList<String> options = new ArrayList();
            for (String s : Arrays.asList("option 1", "option 2", "option 3", "option 4")) {
                options.add(s);
            }
            question.setOptions(options);
            question.setAnswer(options.get(new Random().nextInt(options.size())));
            questions.add(question);
        }
    }

    public void startQuizz(IGameListener listener) {
        if (isRunning) {
            return;
        }

        //initGame(); // just a sample of Game, not the real one
        if (!isRunning) {
            this.listener = listener;
            index = 0;
            duration = 0L;
            score = 0;
            startNextQuestion();
            isRunning = true;
        }
    }

    public void startNextQuestion() {
        startQuestionTime = new Date();

        timer.start();

        if (listener != null) {
            listener.onNewQuestion();
        }
        index ++;
    }

    public Question getCurrentQuestion() {
        return index != -1 && index < questions.size() ? questions.get(index) : null;
    }

    public boolean onAnswer(String answer) {
        boolean goodAnswer = false;

        long answerDurationMs = new Date().getTime() - startQuestionTime.getTime();

        // No answer has been given in time
        if(answer == null) {
            answerDurationMs = Constants.QUESTION_DURATION_MS;
        }

        timer.cancel();
        duration += answerDurationMs;

        if (answer != null) {
            Question currentQuestion = getCurrentQuestion();
            if (answer.equalsIgnoreCase(currentQuestion.getAnswer())) {
                score += duration < Constants.FAST_ANSWER_DELAY ? 2 : 1;
                goodAnswer = true;
            }
        }

        if (index == questions.size() - 1 && listener != null) {
            isRunning = false;
            listener.onGameEnded();
        }

        if (listener != null) {
            listener.onQuestionEnded();
        }

        return goodAnswer;
    }

    public interface IGameListener {
        void onNewQuestion();
        void onQuestionEnded();
        void onGameEnded();
        void onTick(int secondsLeft);
    }
}
