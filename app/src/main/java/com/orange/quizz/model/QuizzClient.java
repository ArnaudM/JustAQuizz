package com.orange.quizz.model;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by LHMF8286 on 22/11/2017.
 */

public interface QuizzClient {

    @GET("questions_simple")
    Call<Quizz> listSimpleQuestions();

    @GET("questions_normal")
    Call<Quizz> listNormalQuestions();

    @GET("questions_expert")
    Call<Quizz> listExpertQuestions();
}
