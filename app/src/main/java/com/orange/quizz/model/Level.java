package com.orange.quizz.model;

/**
 * Created by LHMF8286 on 20/11/2017.
 */

public enum Level {
     BEGINNER, INTERMEDIATE, EXPERT;
}
