package com.orange.quizz.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.orange.quizz.database.Converters;

import java.util.Date;

/**
 * Created by LHMF8286 on 23/11/2017.
 */

@Entity(tableName = "game_result")
public class GameResult {
    private String pseudo;
    private int score;
    private Long duration;

    @TypeConverters({Converters.class})
    private Date date;

    @TypeConverters({Converters.class})
    private Level level;

    @PrimaryKey(autoGenerate = true)
    private int id;

    public String getPseudo() {
        return pseudo;
    }
    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public int getScore() {
        return score;
    }
    public void setScore(int score) {
        this.score = score;
    }

    public Long getDuration() {
        return duration;
    }
    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }

    public Level getLevel() {
        return level;
    }
    public void setLevel(Level level) {
        this.level = level;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GameResult that = (GameResult) o;

        if (score != that.score) return false;
        if (pseudo != null ? !pseudo.equals(that.pseudo) : that.pseudo != null) return false;
        if (duration != null ? !duration.equals(that.duration) : that.duration != null)
            return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        return level == that.level;
    }

    @Override
    public int hashCode() {
        int result = pseudo != null ? pseudo.hashCode() : 0;
        result = 31 * result + score;
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (level != null ? level.hashCode() : 0);
        return result;
    }
}
