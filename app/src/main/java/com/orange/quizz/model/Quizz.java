package com.orange.quizz.model;

import java.util.ArrayList;

/**
 * Created by LHMF8286 on 22/11/2017.
 */

public class Quizz {

    private ArrayList<Question> questions;

    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
    }
}
