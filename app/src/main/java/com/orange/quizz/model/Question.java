package com.orange.quizz.model;

import java.util.ArrayList;

/**
 * Created by LHMF8286 on 20/11/2017.
 */

public class Question {

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    private String question;

    private ArrayList<String> options;

    private String answer;

    private String story;

    private Level level;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public String getStory() { return story; }

    public void setOptions(ArrayList<String> options) {
        this.options = options;
    }

    public ArrayList<String> getOptions() {
        return options;
    }

}
