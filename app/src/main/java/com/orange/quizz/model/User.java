package com.orange.quizz.model;

/**
 * Created by LHMF8286 on 20/11/2017.
 */

public class User {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
