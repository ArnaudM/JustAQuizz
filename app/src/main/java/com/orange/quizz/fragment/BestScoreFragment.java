package com.orange.quizz.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.orange.quizz.R;
import com.orange.quizz.manager.PreferenceManager;

public class BestScoreFragment extends Fragment {

    public BestScoreFragment() {
        // Required empty public constructor
    }

    public static BestScoreFragment newInstance() {
        BestScoreFragment fragment = new BestScoreFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private TextView textviewHighscore;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_best_score, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        textviewHighscore = view.findViewById(R.id.textviewHighscore);

        int score = PreferenceManager.getHighScore(view.getContext());

        String textScore = "High score is " + String.valueOf(score); //TODO format
        textviewHighscore.setText(textScore);
    }
}
