package com.orange.quizz.util;

/**
 * Created by LHMF8286 on 20/11/2017.
 */

public class Constants {

    public static final int NB_QUESTION = 10;

    public static final int QUESTION_DURATION_MS = 10000;

    public static final int FAST_ANSWER_DELAY = 3000;

    public static final String WS_URL = "http://private-f7f742-orangequizz.apiary-mock.com/";
}
