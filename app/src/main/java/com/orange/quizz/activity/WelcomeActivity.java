package com.orange.quizz.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.orange.quizz.R;
import com.orange.quizz.fragment.BestScoreFragment;
import com.orange.quizz.manager.GameDataManager;
import com.orange.quizz.model.Level;

public class WelcomeActivity extends AppCompatActivity
        implements GameDataManager.ILoaderListener,
                   View.OnClickListener,
                   RadioGroup.OnCheckedChangeListener {

    private static final String TAG = WelcomeActivity.class.getName();

    private Level levelChecked = Level.BEGINNER;
    private boolean pseudoTyped = false;

    private static final int QUESTIONS_LOADING = 42;
    private EditText editTextPseudo;
    private RadioGroup radiogroupLevel;
    private RadioButton radiobuttonSimple;
    private RadioButton radiobuttonNormal;
    private RadioButton radiobuttonExpert;
    private Button buttonStart;
    private Button buttonDisplayScore;
    private Button buttonShowHighscore;
    private ProgressBar progressBarSpinner;

    private LinearLayout linearLayoutWelcome;
    private LinearLayout linearLayoutPreWelcome;

    public static final String PARAM_EXTRA_KEY_LEVEL ="levelChecked";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_welcome);
        initViews();
        //GameDataManager.getInstance().launchDatabaseRetrieving(handler, QUESTIONS_LOADING, getApplicationContext());
        GameDataManager.getInstance().initDatabases(getApplicationContext(), this);
    }

    private void initViews() {
        editTextPseudo = findViewById(R.id.editTextPseudo);
        radiogroupLevel = findViewById(R.id.radiogroupLevel);
        radiobuttonSimple = findViewById(R.id.radiobuttonSimple);
        radiobuttonNormal = findViewById(R.id.radiobuttonNormal);
        radiobuttonExpert = findViewById(R.id.radiobuttonExpert);
        buttonStart = findViewById(R.id.buttonStart);
        linearLayoutPreWelcome = findViewById(R.id.linearLayoutPreWelcome);
        progressBarSpinner = findViewById(R.id.progressBarSpinner);
        linearLayoutWelcome = findViewById(R.id.linearLayoutWelcome);
        buttonDisplayScore = findViewById(R.id.buttonDisplayScore);
        buttonShowHighscore = findViewById(R.id.buttonShowHighScore);

        buttonStart.setOnClickListener(this);
        buttonDisplayScore.setOnClickListener(this);
        buttonShowHighscore.setOnClickListener(this);
        radiogroupLevel.setOnCheckedChangeListener(this);

        linearLayoutPreWelcome.setVisibility(View.VISIBLE);
        linearLayoutWelcome.setVisibility(View.GONE);

        editTextPseudo.setSingleLine(true);
        editTextPseudo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length() != 0) {
                    pseudoTyped = true;
                } else {
                    pseudoTyped = false;
                }
                activateStartButton();
            }
        });
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {

        switch(i) {

            case R.id.radiobuttonSimple:
                levelChecked = Level.BEGINNER;
                break;

            case R.id.radiobuttonNormal:
                levelChecked = Level.INTERMEDIATE;
                break;

            case R.id.radiobuttonExpert:
                levelChecked = Level.EXPERT;
                break;

            default: return;
        }
    }

    private Handler handler = new Handler(Looper.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                switch (msg.what){
                    case QUESTIONS_LOADING :
                        Intent mainActivity = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(mainActivity);
                        finish();
                        break;
                }
                super.handleMessage(msg);
            }
        };

    @Override
    public void onProgressChanged(int nbReq) {
        Log.i(TAG, "Request number " + nbReq);
        progressBarSpinner.setProgress(nbReq * 100 / 3);
    }

    @Override
    public void onLoaded(){
        linearLayoutPreWelcome.setVisibility(View.GONE);
        linearLayoutWelcome.setVisibility(View.VISIBLE);
        activateStartButton();
    }

    private void activateStartButton() {
        if (editTextPseudo.getText().length() != 0 && pseudoTyped) {
            buttonStart.setEnabled(true);
        } else {
            buttonStart.setEnabled(false);
        }
    }

    private void showHighScore() {
        BestScoreFragment fragment = (BestScoreFragment) getSupportFragmentManager().findFragmentById(R.id.highscoreFrame);

        fragment = BestScoreFragment.newInstance();

        // Execute a transaction, replacing any existing fragment
        // with this one inside the frame.
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.fragment_container, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.buttonStart:
                Intent mainActivity = new Intent(getApplicationContext(), MainActivity.class);
                mainActivity.putExtra(PARAM_EXTRA_KEY_LEVEL, levelChecked.ordinal());
                startActivity(mainActivity);
                finish();
                break;

            case R.id.buttonDisplayScore:
                Intent displayScoreActivity = new Intent(getApplication(), ScoreDisplayActivity.class);
                startActivity(displayScoreActivity);
                break;

            case R.id.buttonShowHighScore:
                showHighScore();
                break;

            default:
                break;
        }
    }
}
