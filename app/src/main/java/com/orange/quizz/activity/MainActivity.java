package com.orange.quizz.activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.orange.quizz.R;
import com.orange.quizz.manager.PreferenceManager;
import com.orange.quizz.model.Game;
import com.orange.quizz.manager.GameDataManager;
import com.orange.quizz.model.Level;
import com.orange.quizz.model.Question;
import com.orange.quizz.util.Constants;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements Game.IGameListener, View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private static final String TAG = MainActivity.class.getName();
    private Game currentGame;
    private Level levelChosen = Level.BEGINNER;

    private RadioGroup radioGroupOptions;
    private RadioButton radioButtonOption1;
    private RadioButton radioButtonOption2;
    private RadioButton radioButtonOption3;
    private RadioButton radioButtonOption4;
    private TextView textViewQuestionLabel;
    private TextView textViewScore;
    private Button buttonNext;
    private ProgressBar progressBarTimer;

    Intent scoreIntent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();

        Log.i(TAG, "onCreate");
        currentGame = new Game();

        //GameDataManager.getInstance().initDatabases(this);
        switch(levelChosen) {
            case BEGINNER:
                currentGame.setQuestions(GameDataManager.getInstance().getSimpleQuestions().getQuestions());
                break;

            case INTERMEDIATE:
                currentGame.setQuestions(GameDataManager.getInstance().getIntermediateQuestions().getQuestions());
                break;

            case EXPERT:
                currentGame.setQuestions(GameDataManager.getInstance().getExpertQuestions().getQuestions());
                break;

            default:
                currentGame.setQuestions(GameDataManager.getInstance().getSimpleQuestions().getQuestions());
                break;
        }

        currentGame.startQuizz(this);
    }

    private void initViews() {
        radioGroupOptions = findViewById(R.id.radioGroupOptions);
        radioButtonOption1 = findViewById(R.id.radioButtonOption1);
        radioButtonOption2 = findViewById(R.id.radioButtonOption2);
        radioButtonOption3 = findViewById(R.id.radioButtonOption3);
        radioButtonOption4 = findViewById(R.id.radioButtonOption4);
        textViewQuestionLabel = findViewById(R.id.textViewQuestionLabel);
        textViewScore = findViewById(R.id.textViewScore);
        buttonNext = findViewById(R.id.buttonNext);
        progressBarTimer = findViewById(R.id.progressBarTimer);

        buttonNext.setVisibility(View.GONE);
        buttonNext.setBackgroundColor(Color.GRAY);
        buttonNext.setOnClickListener(this);

        radioGroupOptions.setOnCheckedChangeListener(this);
        /*radioButtonOption1.setOnClickListener(this);
        radioButtonOption2.setOnClickListener(this);
        radioButtonOption3.setOnClickListener(this);
        radioButtonOption4.setOnClickListener(this);*/

        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            int index = extras.getInt(WelcomeActivity.PARAM_EXTRA_KEY_LEVEL);
            levelChosen = Level.values()[index];
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.i(TAG, "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();

        //currentGame.startQuizz(this);
        Log.i(TAG, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.i(TAG, "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.i(TAG, "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.i(TAG, "onDestroy");
    }

    @Override
    public void onNewQuestion() {
        Question currentQuestion = currentGame.getCurrentQuestion();

        if (currentQuestion != null) {
            Log.i(TAG, "New Question :\n" +
                    currentQuestion.getQuestion() + "\n");

            textViewScore.setText(Integer.toString(currentGame.getScore()));

            textViewQuestionLabel.setText(currentQuestion.getQuestion());

            ArrayList<String> options = currentQuestion.getOptions();


            radioButtonOption1.setText(options.get(0));
            radioButtonOption2.setText(options.get(1));
            radioButtonOption3.setText(options.get(2));
            radioButtonOption4.setText(options.get(3));
        }

    }

    @Override
    public void onQuestionEnded() {
        Log.i(TAG, "Question ended \n");
        radioGroupOptions.clearCheck();
        progressBarTimer.setVisibility(View.GONE);
        buttonNext.setVisibility(View.VISIBLE);
    }

    @Override
    public void onGameEnded(){

        Log.v(TAG, "Game ended \n");
        PreferenceManager.updateHighScore(this, currentGame.getScore());

        scoreIntent = new Intent(this, ScoreActivity.class);
        scoreIntent.putExtra("score", currentGame.getScore());
        startActivity(scoreIntent);
    }

    @Override
    public void onTick(int secondsLeft) {
        Log.i(TAG, "Time left :" + secondsLeft + "s");

        int progress = Constants.QUESTION_DURATION_MS - (secondsLeft * 1000);
        progress /= progressBarTimer.getMax();

        progressBarTimer.setProgress(progress);
    }

    @Override
    public void onClick(View view) {
        boolean isOk = false;

        switch(view.getId()) {
            case R.id.buttonNext:
                currentGame.startNextQuestion();
                progressBarTimer.setVisibility(View.VISIBLE);
                buttonNext.setVisibility(View.GONE);
                break;
/*
            case R.id.radioButtonOption1:
                isOk = currentGame.onAnswer(radioButtonOption1.getText().toString());
                if (isOk) {
                    buttonNext.setBackgroundColor(Color.GREEN);
                    buttonNext.setVisibility(View.VISIBLE);
                } else {
                    buttonNext.setBackgroundColor(Color.RED);
                    buttonNext.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.radioButtonOption2:
                isOk = currentGame.onAnswer(radioButtonOption2.getText().toString());
                if (isOk) {
                    buttonNext.setBackgroundColor(Color.GREEN);
                    buttonNext.setVisibility(View.VISIBLE);
                } else {
                    buttonNext.setBackgroundColor(Color.RED);
                    buttonNext.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.radioButtonOption3:
                isOk = currentGame.onAnswer(radioButtonOption3.getText().toString());
                if (isOk) {
                    buttonNext.setBackgroundColor(Color.GREEN);
                    buttonNext.setVisibility(View.VISIBLE);
                } else {
                    buttonNext.setBackgroundColor(Color.RED);
                    buttonNext.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.radioButtonOption4:
                isOk = currentGame.onAnswer(radioButtonOption4.getText().toString());
                if (isOk) {
                    buttonNext.setBackgroundColor(Color.GREEN);
                    buttonNext.setVisibility(View.VISIBLE);
                } else {
                    buttonNext.setBackgroundColor(Color.RED);
                    buttonNext.setVisibility(View.VISIBLE);
                }
                break;
*/
            default: break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        boolean isOk = false;

        switch(i) {

            case R.id.radioButtonOption1:
                isOk = currentGame.onAnswer(radioButtonOption1.getText().toString());
                break;

            case R.id.radioButtonOption2:
                isOk = currentGame.onAnswer(radioButtonOption2.getText().toString());
                break;

            case R.id.radioButtonOption3:
                isOk = currentGame.onAnswer(radioButtonOption3.getText().toString());
                break;

            case R.id.radioButtonOption4:
                isOk = currentGame.onAnswer(radioButtonOption4.getText().toString());
                break;

            default: return;
        }

        if (isOk) {
            Log.i(TAG, "isOk = " + isOk);
        }
    }
}
