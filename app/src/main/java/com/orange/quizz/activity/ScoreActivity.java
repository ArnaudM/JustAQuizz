package com.orange.quizz.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.orange.quizz.R;
import com.orange.quizz.model.Game;

public class ScoreActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = ScoreActivity.class.getName();

    private TextView textViewScore;
    private Button buttonReturn;
    private int scoreToDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        initViews();

        Log.i(TAG, "onCreate");
    }

    private void initViews() {
        textViewScore = findViewById(R.id.textViewScore);
        buttonReturn = findViewById(R.id.buttonReturn);

        Bundle extras = getIntent().getExtras();

        if (extras != null ) {
           scoreToDisplay = extras.getInt("score");

            textViewScore.setText(getString(R.string.success_sentence, scoreToDisplay));
        }

        buttonReturn.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        Intent mainActivity = new Intent(this, MainActivity.class);
        startActivity(mainActivity);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
        return true;
    }

    private void shareScore() {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_score, scoreToDisplay));
        shareIntent.setType("text/plain");
        if(shareIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(shareIntent);
        } else {
            Toast.makeText(this, R.string.toast_no_share_app, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
         switch (item.getItemId()) {

             case R.id.share_action:
                 shareScore();
                 return true;

             default:
                 return super.onOptionsItemSelected(item);
         }
    }
}
