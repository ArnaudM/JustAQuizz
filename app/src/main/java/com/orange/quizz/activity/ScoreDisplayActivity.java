package com.orange.quizz.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.orange.quizz.R;
import com.orange.quizz.adapter.GameResultAdapter;
import com.orange.quizz.database.DatabaseManager;
import com.orange.quizz.model.GameResult;

import java.util.ArrayList;

public class ScoreDisplayActivity extends AppCompatActivity {

    private RecyclerView recyclerviewScores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score_display);
        recyclerviewScores = findViewById(R.id.recyclerviewScores);
        recyclerviewScores.setLayoutManager(new LinearLayoutManager(this, LinearLayout.VERTICAL, false));
        DatabaseManager.getInstance(this).getGameResults(new DatabaseManager.IGameResultListener() {
            @Override
            public void onGameResultsRetrieved(ArrayList<GameResult> results) {
                GameResultAdapter adapter = new GameResultAdapter(results);
                recyclerviewScores.setAdapter(adapter);
            }
        });
    }
}
