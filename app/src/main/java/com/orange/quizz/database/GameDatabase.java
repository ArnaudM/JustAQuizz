package com.orange.quizz.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.orange.quizz.model.GameResult;

/**
 * Created by LHMF8286 on 23/11/2017.
 */

@Database(entities = {GameResult.class}, version = 1)
public abstract class GameDatabase extends RoomDatabase {

    public static final String DB_NAME = "game_db";

    public abstract GameResultDao getGameResultDao();
}
