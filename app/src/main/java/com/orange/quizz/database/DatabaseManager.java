package com.orange.quizz.database;

import android.annotation.SuppressLint;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.orange.quizz.model.GameResult;

import java.util.ArrayList;

/**
 * Created by LHMF8286 on 23/11/2017.
 */

public class DatabaseManager {
    private static DatabaseManager ourInstance = null;

    private GameDatabase database;

    private Context context;

    public static DatabaseManager getInstance(Context context) {

        if (ourInstance == null) {
            ourInstance = new DatabaseManager(context);
        }

        return ourInstance;
    }

    private DatabaseManager(Context context) {
        this.context = context.getApplicationContext();
        this.database = Room.databaseBuilder(context, GameDatabase.class, GameDatabase.DB_NAME).build();
    }

    public void storeGameResult(final GameResult gameResult) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                long id = database.getGameResultDao().insertResult(gameResult);
                Log.e("Database", "Id : " +id);
            }
        }).start();
    }

    @SuppressLint("StaticFieldLeak")
    public void getGameResults(final IGameResultListener listener) {
            new AsyncTask<Void, Void, ArrayList<GameResult>>() {
                @Override
                protected void onPostExecute(ArrayList<GameResult> gameResults) {
                    if(listener != null) {
                        listener.onGameResultsRetrieved(gameResults);
                    }
                }

                @Override
                protected ArrayList<GameResult> doInBackground(Void... voids) {
                    return new ArrayList<>(database.getGameResultDao().getAllResults());
                }
            }.execute();
    }

    public interface IGameResultListener {
        void onGameResultsRetrieved(ArrayList<GameResult> results);
    }
}
