package com.orange.quizz.database;

import android.arch.persistence.room.TypeConverter;

import com.orange.quizz.model.Level;

import java.util.Date;

/**
 * Created by LHMF8286 on 23/11/2017.
 */

public class Converters {
    @TypeConverter
    public Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public Long dateToTimestamp(Date date) {
        if (date == null) {
            return null;
        } else {
            return date.getTime();
        }
    }

    @TypeConverter
    public Level fromInteger(int index) {
        return index < 3 ? Level.values()[index] : Level.BEGINNER;
    }

    @TypeConverter
    public int levelToInt(Level l) {
        return l.ordinal();
    }
}
