package com.orange.quizz.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.orange.quizz.model.GameResult;

import java.util.List;

/**
 * Created by LHMF8286 on 23/11/2017.
 */

@Dao
public interface GameResultDao {
    @Insert
    long insertResult(GameResult gameResult);

    @Query("SELECT * from game_result")
    List<GameResult> getAllResults();
}
