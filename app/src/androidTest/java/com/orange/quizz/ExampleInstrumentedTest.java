package com.orange.quizz;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.orange.quizz.database.DatabaseManager;
import com.orange.quizz.model.GameResult;
import com.orange.quizz.model.Level;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void checkDatabase() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        DatabaseManager manager = DatabaseManager.getInstance(appContext);

        final ArrayList<GameResult> games = new ArrayList<>();

        for (int i = 0; i < 20; i++) {
            GameResult result = new GameResult();
            result.setDate(new Date());
            result.setPseudo("Pseudo_" + i);
            result.setScore(1000 * i);
            result.setDuration(12000L);
            result.setLevel(Level.EXPERT);
            games.add(result);
            manager.storeGameResult(result);
        }

        Thread.sleep(1000);

        manager.getGameResults(new DatabaseManager.IGameResultListener() {
            @Override
            public void onGameResultsRetrieved(ArrayList<GameResult> results) {
                for (GameResult result : results) {
                    Log.i("TEST Result :", result.getPseudo());
                    assertTrue(games.contains(result));
                }
            }
        });
    }
}
